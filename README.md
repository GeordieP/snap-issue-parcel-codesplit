- `yarn start` or `npm start` to run in parcel dev mode

- `yarn clean` or `npm run clean` to remove the parcel dist/ (dev mode), build/, and .cache/ directories

- `yarn build` or `npm run build` to run `clean` and build the app with parcel and run react-snap

This repo *mostly* works as it should, with the app loading correctly, JavaScript executing correctly, and code splitting working; however, there is an error in the console:

`GET http://localhost:45678/First.f29ce38c.js net::ERR_CONNECTION_REFUSED`

react-snap adds the following line to the page head, which contains an address and port that we're not using in the built app:

`<script src="http://localhost:45678/First.f29ce38c.js" async="" charset="utf-8" type="text/javascript"></script>`

However, when the app is running, this line is also present in the head:

`<script async="" type="text/javascript" charset="utf-8" src="http://localhost:5000/First.f29ce38c.js"></script>`

(I'm using [serve](https://github.com/zeit/serve) to serve the `build/` directory after it's generated, and it runs on port 5000)

If we manually remove the first script tag from the generated `build/index.html` file, and we leave the second, everything appears to work properly (though I have not yet tried this on a real server).
