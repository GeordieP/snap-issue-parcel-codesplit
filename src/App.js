import React, { Component } from 'react';
import { Router, Link } from '@reach/router';
import loadable from 'loadable-components';

const AsyncHome = loadable(() => import('./First'));
const AsyncSecond = loadable(() => import('./Second'))

export default () => (
  <div>
    <Link to='/'>Home</Link>{' | '}
    <Link to='/second'>Second Page</Link>

    <Router>
      <AsyncHome path='/' />
      <AsyncSecond path='/second' />
    </Router>
  </div>
);
