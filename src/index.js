import React from 'react';
import { hydrate, render } from "react-dom";
import { loadComponents, getState } from 'loadable-components';

window.snapSaveState = () => getState();

import App from './App';

const rootElement = document.getElementById("root");

if (rootElement.hasChildNodes()) {
  loadComponents().then(() => {
    hydrate(<App />, rootElement);
  });
} else {
  render(<App />, rootElement);
}
